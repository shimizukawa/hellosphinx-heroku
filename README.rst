Hello Sphinx!
===============

This is a practice of "Hello Sphinx" on Heroku with basic-auth.

1. setup heroku project

2. init git repository and add files as:

   - https://gitlab.com/shimizukawa/hellosphinx-heroku

3. push to Heroku git:

   - ``git@heroku.com:<YOUR-HEROKU-PROJECT-NAME>.git``

4. Done!

Generated page: https://hellosphinx-heroku.herokuapp.com/  (ID/PW = hello / sphinx)


-------------

If you want to manage yore repository on GitLab (it provide free private repository and repository sync feature), extra procedure is required.

5. Get yoru Heroku API key:

   - On Heroku: ``Account Settings`` -> ``Account`` -> ``API Key`` -> Reveal

6. create gitlab project

   - As https://gitlab.com/shimizukawa/hellosphinx-heroku

7. setup git push of gitlab

   - On Gitlab: ``settings`` -> ``Repository`` -> ``Push to a remote repository``
   - check: ``Remote mirror repository``
   - set: ``Git repository URL`` as ``https://heroku:<API-TOKEN-OF-HEROKU>@git.heroku.com/<HEROKU-APP-NAME>.git``

   .. figure:: gitlab-gitsync.*

8. Push yore repository to gitlab.

9. Done!


